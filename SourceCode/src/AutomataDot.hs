{-# LANGUAGE OverloadedStrings #-}
module AutomataDot(getSuspAutFromLTS, getSuspAutFromMealy) where

import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as B
import Data.Char (isSpace)
import qualified Data.Map as Map
import Data.Map (Map, (!))
import qualified Data.Set as Set
import Data.Set (Set)
import System.IO
import qualified Data.List as List
import qualified Data.Foldable as Foldable
import qualified Data.Text as Text

import ParseDot
import MealyMoore
import qualified Aut
import qualified SuspAut
import InputSelection as InputSelection (InputSelection)



getDotLTS :: String -> String -> IO DotGraph
getDotLTS fileReadDir readFileName = do
    cont <- readFile (fileReadDir ++ readFileName)
    case parseDotBS (B.pack cont) of
        Left err -> error err
        Right val -> return val

getSuspAutFromDot :: (String -> Bool) -> DotGraph -> SuspAut.SuspAut
getSuspAutFromDot isInput dot =
    let transList = [(toState f, B.unpack $ snd $ head attrs, toState t) | E f t attrs <- statements dot]
        init = let (i,_,_) = head transList in i
        inputs = Set.fromList [mu | (_,mu,_) <- transList, isInput mu ]
        outputs = Set.fromList [mu | (_,mu,_) <- transList, not(isInput mu) ]
    in Aut.addDelta "delta" $ Aut.constrAut (init,Set.fromList transList, inputs, outputs)


getSuspAutFromLTS ::  (String -> Bool) -> String -> String -> IO SuspAut.SuspAut
getSuspAutFromLTS isInput fileReadDir readFileName = do
    dot <- getDotLTS fileReadDir readFileName
    return $ getSuspAutFromDot isInput dot


-- create Mealy machine where states, input and output are all bytestrings
type BSMealy = Mealy ByteString ByteString ByteString


getSuspAutFromMealy :: (String -> Bool) -> InputSelection -> String -> String -> String -> IO SuspAut.SuspAut
getSuspAutFromMealy isInput inpSel delta fileReadDir readFileName = do
    autParts <- getMealyPartsFromDot inpSel fileReadDir readFileName
    return $ mealyToSuspAut isInput delta autParts

getMealyPartsFromDot :: InputSelection -> String -> String -> IO (BSMealy, Set ByteString, Set ByteString, Set ByteString, [((ByteString,ByteString),(ByteString,ByteString))])
getMealyPartsFromDot inpSel fileReadDir readFileName =
    withFile (fileReadDir ++ readFileName) ReadMode (readTheFile inpSel)

-- Only works for deterministic mealy machines.
-- Also returns the alphabet
convertDotToMealy :: (ByteString -> ByteString -> ByteString -> ByteString -> Bool) -> DotGraph -> (BSMealy, Set ByteString, Set ByteString, Set ByteString, [((ByteString,ByteString),(ByteString,ByteString))])
convertDotToMealy mealyRestrictions dot = (mealy, inputs, outputs, states,transitions) where
    label attrs  = take 1 [splitLabel v | (k, v) <- attrs, k == "label"]
    splitLabel l = let [i, o] = B.split '/' l in (trim i, trim o)
    trim         = B.takeWhile (not . isSpace) . B.dropWhile isSpace
    transitions  = [((s, i), (t, o)) | E s t attrs <- statements dot, (i, o) <- label attrs, mealyRestrictions s i t o]
    table        = Map.fromList transitions
    inputs     = Set.fromList [i | ((_, i), _) <- transitions]
    outputs     = Set.fromList [o | (_,(_,o)) <- transitions]
    states      = Set.union (Set.fromList [s | ((s, _), _) <- transitions]) (Set.fromList [t | (_,(t,_)) <- transitions])
    mealy        = Mealy
      { initT  = fst . fst . head $ transitions -- take first-read state as initial state
      , deltaT = \s i -> table ! (s, i)         -- look up transition
      }


readDotAsMealy :: (ByteString -> ByteString -> ByteString -> ByteString -> Bool) -> ByteString -> Either String (BSMealy, Set ByteString, Set ByteString, Set ByteString,[((ByteString,ByteString),(ByteString,ByteString))])
readDotAsMealy mealyRestrictions = fmap (convertDotToMealy mealyRestrictions) . parseDotBS

readTheFile :: (ByteString -> ByteString -> ByteString -> ByteString -> Bool) -> Handle -> IO (BSMealy, Set ByteString, Set ByteString, Set ByteString,[((ByteString,ByteString),(ByteString,ByteString))])
readTheFile mealyRestrictions handle = do
    cont <- hGetContents handle
    let bla = readDotAsMealy mealyRestrictions (B.pack cont)
    case bla of
        Left err -> error err
        Right val -> return val

mealyToSuspAut ::  (String -> Bool) -> String -> (BSMealy, Set ByteString, Set ByteString, Set ByteString, [((ByteString,ByteString),(ByteString,ByteString))]) -> SuspAut.SuspAut
mealyToSuspAut isInput delta (m, inputs, outputs, states, trans) =
    let groupedTrans = groupTrans $ transToRightType trans
        (nondetTrans,nr) = toNonDetLTS groupedTrans (Set.size states)
        nonDetTransMap = getTransMap $ addDeltaTrans (Set.size states) delta $ removeDeltaTrans delta $ nondetTrans
        -- suspTrans = Trace.trace ("nr=" ++ (show nr)) (determinizeLTS (toState (Automata.initT m)) nr $ getTransMap $ addDeltaTrans (Set.size states) delta $ removeDeltaTrans delta $ nondetTrans)
    in intSetAutToSuspAut $ determinize nonDetTransMap (toState (MealyMoore.initT m)) isInput

transToRightType :: [((ByteString,ByteString),(ByteString,ByteString))] -> [((Int,String),(Int,String))]
transToRightType trans =
    map (\t -> case t of ((s,i),(t,o)) -> ((toState s, toInput i),(toState t, toOutput o))) trans

getMealyFromByteString :: ByteString -> Set ByteString -> Set ByteString -> Set ByteString -> [((ByteString,ByteString),(ByteString,ByteString))]
                          -> (Int, Set String, Set String, Set Int, [((Int,String),(Int,String))])
getMealyFromByteString initial inputs outputs states trans =
    (toState initial, Set.map toInput inputs, Set.map toOutput outputs, Set.map toState states, transToRightType trans)

toInput :: ByteString -> String
toInput b =  "I" ++ (B.unpack b)
toOutput :: ByteString -> String
toOutput b = "O" ++ (B.unpack b)
toState :: ByteString -> Int
toState b = read $ tail $ B.unpack b

toNonDetLTS :: Map Int (Map String (Set (((Int,String),(Int,String))))) -> Int -> (Set (Int,String,Int),Int)
toNonDetLTS stateMap newStateId =
    Map.foldr (\transMap tuple ->
        Map.foldrWithKey (\inp transSet tup ->
            Set.foldr (\trans tup' ->
                case tup' of
                    (set,nr) -> case trans of
                        ((s,_),(t,o)) -> if isMultiOutput o
                                            then outputListElToTrans nr (getMultiOutput o) (Set.insert (s,inp,nr) set) t
                                            else (Set.insert (nr,o,t) $ Set.insert (s,inp,nr) set,nr+1)
                ) tup transSet
            ) tuple transMap
        ) (Set.empty,newStateId) stateMap

removeDeltaTrans :: String -> Set (Int,String,Int) -> Set (Int,String,Int)
removeDeltaTrans delta trans =
    Set.foldr (\tr trans -> case tr of
                                (s1,o,t1) ->
                                    if o == delta
                                    then case Foldable.find (\tr0 -> case tr0 of (s0,i,t0) -> t0 == s1 && s0 == t1) trans of
                                            Nothing -> trans
                                            Just tr0 -> case tr0 of (s0,i,t0) -> Set.insert (s0,i,s0) (Set.delete tr (Set.delete tr0 trans))
                                    else trans) trans trans

-- add deltas for all states with id from original mealy machine
addDeltaTrans :: Int -> String -> Set (Int,String,Int) -> Set (Int,String,Int)
addDeltaTrans nrStates delta trans = addDeltaTrans' (nrStates-1) delta trans where
    addDeltaTrans' stateId delta trans = if stateId >= 0 then addDeltaTrans' (stateId-1) delta (Set.insert (stateId, delta, stateId) trans) else trans

getTransMap :: Set (Int,String,Int) -> Map Int (Map String (Set Int))
getTransMap trans = Set.foldr(\(s,mu,t) tmap -> case Map.lookup s tmap of
                                                    Nothing -> Map.insert s (Map.singleton mu (Set.singleton t)) tmap
                                                    Just mumap -> Map.insert s (Map.insertWith Set.union mu (Set.singleton t) mumap) tmap) Map.empty trans

determinize :: Map Int (Map String (Set Int)) -> Int -> (String -> Bool) -> Aut.Aut (Set Int) String
determinize transMap initial isInput =
    let iniState = getAutState transMap isInput initial
    in Aut.statesToAut iniState $ expandRec transMap isInput (Set.singleton iniState) (Set.singleton iniState)

getAutState :: Map Int (Map String (Set Int)) -> (String -> Bool) -> Int ->  Aut.State (Set Int) String
getAutState transMap isInput state =
    Aut.State
      (Set.singleton state)
      (Set.filter isInput $ Map.keysSet (transMap ! state))
      (Set.filter (not . isInput) $ Map.keysSet (transMap ! state))
      (transMap ! state)

getAutSetState :: Map Int (Map String (Set Int)) -> (String -> Bool) -> Set Int ->  Aut.State (Set Int) String
getAutSetState transMap isInput state =
    let mumaps = Set.map (\s -> transMap ! s) state
        stateTrans = (List.foldr1 mergeMuMaps $ Set.toList mumaps)
    in Aut.State
      state
      (Set.filter isInput $ Map.keysSet stateTrans)
      (Set.filter (not . isInput) $ Map.keysSet stateTrans)
      stateTrans

mergeMuMaps :: Map String (Set Int) -> Map String (Set Int) -> Map String (Set Int)
mergeMuMaps m1 m2 = List.foldr (\(mu1,set1) m2 -> Map.insertWith Set.union mu1 set1 m2) m2 (Map.toList m1)

expand :: Aut.State (Set Int) String -> Map Int (Map String (Set Int)) -> (String -> Bool) -> Set (Aut.State (Set Int) String) -> Set (Aut.State (Set Int) String)
expand state transMap isInput foundStates =
    let newStates = Set.map (getAutSetState transMap isInput) $ Set.fromList $ Map.elems $ Set.foldr (\mu muIntMap -> Map.insertWith Set.union mu ((Aut.trans state) ! mu) muIntMap) Map.empty (Aut.enab state)
    -- Set.map (getAutState transMap isInput) $ Set.foldr (\mu intSet -> Set.union intSet $ (Aut.trans state) ! mu) Set.empty (Aut.enab state)
    in newStates Set.\\ foundStates

expandRec :: Map Int (Map String (Set Int)) -> (String -> Bool) -> Set (Aut.State (Set Int) String) -> Set (Aut.State (Set Int) String) -> Set (Aut.State (Set Int) String)
expandRec transMap isInput foundStates newStates =
    if Set.null newStates then foundStates
    else let state = (Set.elemAt 0 newStates)
             moreNewStates = expand state transMap isInput foundStates
         in expandRec transMap isInput (Set.union foundStates moreNewStates) (Set.union moreNewStates (Set.delete state newStates))

intSetAutToSuspAut :: Aut.Aut (Set Int) String -> SuspAut.SuspAut
intSetAutToSuspAut aut =
    let iniNr = Set.elemAt 0 $ Aut.sid $ Aut.initial aut
        stateNrMap = fst $ Set.foldr (\state (map,nr) -> (Map.insert state nr map, nr+1))
                                     (Map.singleton (Aut.initial aut) iniNr,iniNr+1)
                                     (Set.delete (Aut.initial aut) (Aut.states aut)) :: Map (Aut.State (Set Int) String) Int
        newStatesWithId = Set.map (\s -> (stateNrMap ! s, Aut.State (stateNrMap ! s)
                                                                    (Aut.inp s)
                                                                    (Aut.out s)
                                                                    (Map.map (\dest -> stateNrMap ! (case Map.lookup dest $ Aut.idStateMap aut of {Nothing -> error ((show dest) ++ (show $ Set.member dest $ Set.map Aut.sid $ Aut.states aut)); Just k -> k})) (Aut.trans s)))) (Aut.states aut) :: Set (Int,Aut.State Int String)
        newIdStateMap = (Map.fromList $ Set.toList $ newStatesWithId) :: Map Int (Aut.State Int String)
    in Aut.Aut (newIdStateMap ! (stateNrMap ! Aut.initial aut)) (Set.map snd newStatesWithId) newIdStateMap (Aut.inputs aut) (Aut.outputs aut)

-- src-state(new-id) outputs set-to-be-added-to dest-state
outputListElToTrans :: Int -> [String] -> Set (Int,String,Int) -> Int -> (Set (Int,String,Int), Int)
outputListElToTrans nr [o] set t = (Set.insert (nr,o,t) set,nr+1)
outputListElToTrans nr (o:os) set t =  outputListElToTrans (nr+1) os (Set.insert (nr,o,nr+1) set) t

getMultiOutput :: String -> [String]
getMultiOutput o = List.map Text.unpack $ Text.split ((==) '-') (Text.pack o)

isMultiOutput :: String -> Bool
isMultiOutput o = List.elem '-' o

groupTrans :: [((Int,String),(Int,String))] -> Map Int (Map String (Set (((Int,String),(Int,String)))))
groupTrans trans = let stateTrans = List.foldl (\newmap t -> case t of ((s,_),_) -> Map.insertWith (Set.union) s (Set.singleton t) newmap) Map.empty trans :: Map Int (Set (((Int,String),(Int,String))))
                   in Map.map (\tr -> Set.foldl (\newmap t -> case t of ((s,i),_) -> Map.insertWith (Set.union) (removeDotSuffix i) (Set.singleton t) newmap) Map.empty tr) stateTrans

removeDotSuffix :: String -> String
removeDotSuffix i = if List.elem '.' i then takeWhile ((/=) '.') i else i


toStautDefString :: SuspAut.SuspAut -> String
toStautDefString aut =
    let inputList = List.intercalate ", " (Set.toList $ Aut.inputs aut)
        outputList = List.intercalate ", " (Set.toList $ Aut.outputs aut)
        alphabetList = inputList ++ "," ++ outputList
        chans = "CHANDEF  Chans ::= " ++ alphabetList ++   " ENDDEF\n\n"
        modeldef = "MODELDEF  Test ::=\n" ++
                          "\tCHAN IN  " ++ inputList ++ "\n" ++
                          "\tCHAN OUT " ++ outputList ++ "\n" ++
                          "\tBEHAVIOUR stautdef [ " ++ alphabetList ++ " ] ( )\n" ++
                    "ENDDEF\n\n"
        transitions = List.foldr (\s list -> List.foldr (\t list' -> ["s" ++ (show $ Aut.sid s),fst t, "s" ++ (show $ snd t)]:list') list (Map.toList $ Aut.trans s)) [] (Aut.states aut)
        statdef =   "STAUTDEF stautdef [ " ++ alphabetList ++ " ] ( ) ::=\n" ++
                          "\tSTATE s" ++ (List.intercalate ", s" $ (Set.toList $ Set.map (show . Aut.sid) (Aut.states aut))) ++ "\n" ++
                          "\tVAR\n" ++
                          "\tINIT " ++ (show $ Aut.sid $ Aut.initial aut) ++ "\n" ++
                          "\tTRANS\n" ++ "\t\t" ++ (List.intercalate "\n\t\t" (map (List.intercalate " -> ") transitions)) ++ "\n" ++
                    "ENDDEF"
       in chans ++ modeldef ++ statdef