# State Identification Software

This repository accompanies the journal paper State Identification for Labeled Transition Systems with Inputs and Outputs by Petra van den Bos and Frits Vaandrager.
It contains:

- The cource code in directory ```SourCode```
- The compiled code as the executable ```adg-exe```
- Case study automata in .dot format in the directory ```CaseStudyDots``` 
- For each case study, a folder containing the (intermediate) computation results.

To compute or inspect computation results, the executable adg-exe can be used directly, e.g.:

- To compute results for the TCP case study (again), enter:

	```./adg-exe TCP```
	
- To inspect the computed results, enter:

	```./adg-exe TCP exp-text```
	
- To get the entries of Table 1 of the paper for the TCP row, enter

	```./adg-exe TCP aut-table```
	
- To get the entries of Table 2 of the paper for the TCP row, enter

	```./adg-exe TCP exp-table```

To compile the source code, install the Haskell tool Stack (see https://docs.haskellstack.org/en/stable/install_and_upgrade/), and then enter the following commands:

```
cd SourceCode/
stack init
stack install
```
